#!/usr/bin/env bash
set -eu

sudo pacman --sync --needed --noconfirm \
firefox \
gnome \
gnome-extra \
gnome-shell-extension-appindicator \
solaar \
xorg-mkfontscale \
webp-pixbuf-loader

if $(yay --version); then
    sudo pacman --sync --needed --noconfirm libqtxdg
    yay --sync --noconfirm enpass-bin
fi

sudo systemctl enable gdm.service
