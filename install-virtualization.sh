#!/usr/bin/env bash

sudo pacman --sync --needed --noconfirm \
dmidecode \
dnsmasq \
edk2-ovmf \
guestfs-tools \
libosinfo \
libvirt \
qemu-full \
qemu-img \
virt-manager \
virt-viewer

sudo systemctl enable libvirtd.service
sudo systemctl start libvirtd.service
