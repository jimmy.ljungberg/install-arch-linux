#!/usr/bin/env bash
set -eu

export BRANCH="${BRANCH:-main}"
export VOLUME_GROUP="${VOLUME_GROUP:-system}"

BOOT_PARTITION="${BOOT_PARTITION:-1}"
LVM_PARTITION="${LVM_PARTITION:-2}"

function erase_existing_lvm() {
  swapoff -a
  lvs --noheadings --options lv_path | xargs --no-run-if-empty lvremove --yes --force
  vgs --noheadings --options vg_name | xargs --no-run-if-empty vgremove --yes --force
  pvs --noheadings --options pv_name | xargs --no-run-if-empty pvremove --yes --force
}

function partition_storage() {
  parted --script --align optimal ${STORAGE_DEVICE} \
    mklabel gpt \
    mkpart primary fat32 1MiB 1GiB set 1 esp on \
    mkpart primary ext4 1GiB 100%
}

function configure_lvm() {
  pvcreate ${STORAGE_DEVICE}${LVM_PARTITION}

  vgcreate --yes "${VOLUME_GROUP}" ${STORAGE_DEVICE}${LVM_PARTITION}

  lvcreate --yes -L 12G "${VOLUME_GROUP}" -n root
  lvcreate --yes -L 8G "${VOLUME_GROUP}" -n swap
  lvcreate --yes -L 2G "${VOLUME_GROUP}" -n home
}

function create_and_mount_file_systems() {
  mkfs.fat -F32 ${STORAGE_DEVICE}${BOOT_PARTITION}

  mkfs.ext4 /dev/${VOLUME_GROUP}/root
  mkfs.ext4 /dev/${VOLUME_GROUP}/home

  mkswap /dev/${VOLUME_GROUP}/swap
  swapon /dev/${VOLUME_GROUP}/swap

  mount --mkdir /dev/${VOLUME_GROUP}/root /mnt
  mount --mkdir ${STORAGE_DEVICE}${BOOT_PARTITION} /mnt/boot
  mount --mkdir /dev/${VOLUME_GROUP}/home /mnt/home
}

timedatectl set-timezone Europe/Stockholm

findmnt --noheadings --list --real --output TARGET | grep /mnt | sort --reverse | xargs --no-run-if-empty umount

erase_existing_lvm
partition_storage
configure_lvm
create_and_mount_file_systems

pacstrap -K /mnt \
  amd-ucode \
  base \
  linux \
  linux-firmware \
  lvm2 \
  networkmanager \
  vim

genfstab -U /mnt \
| sed 's/fmask=0022/fmask=0137/g' \
| sed 's/dmask=0022/dmask=0027/g' >> /mnt/etc/fstab

arch-chroot /mnt /bin/bash <<END
curl -s $URL/$BRANCH/install-2.sh | bash
END
