#!/usr/bin/env bash
set -eu

cd /tmp
rm -fr yay-git
sudo -u $SUDO_USER git clone https://aur.archlinux.org/yay-git.git
cd yay-git
sudo -u $SUDO_USER makepkg --syncdeps --install --noconfirm
rm -fr yay-git
cd ..
