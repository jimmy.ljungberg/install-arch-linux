#!/usr/bin/env bash
set -eu

HOST_NAME="${HOST_NAME:-pc}"

function add_sudo_user() {
useradd -m -G wheel -s /bin/bash $SUDO_USER

rm -f /etc/sudoers.d/$SUDO_USER
cat <<-EOF >> /etc/sudoers.d/$SUDO_USER
$SUDO_USER   ALL=(ALL)   NOPASSWD:ALL
EOF

chmod 400 /etc/sudoers.d/$SUDO_USER
}

pacman --sync --needed --noconfirm \
base-devel \
bash-completion \
bat \
dkms \
eza \
fd \
fzf \
git \
hwdetect \
jq \
man-db \
man-pages \
neovim \
nerd-fonts \
openssh \
pacman-contrib \
reflector \
rsync \
sudo \
texinfo \
tmux \
tree \
unzip \
wget \
xdg-user-dirs \
xdg-utils \
zip

systemctl enable reflector.timer
systemctl enable sshd.service

ln -sf /usr/share/zoneinfo/Europe/Stockholm /etc/localtime
hwclock --systohc

sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
sed -i 's/#sv_SE.UTF-8/sv_SE.UTF-8/' /etc/locale.gen
locale-gen

echo "LANG=en_US.UTF-8" > /etc/locale.conf
echo "LANGUAGE=en_US:en:C" >> /etc/locale.conf

echo "${HOST_NAME}" > /etc/hostname

rm -f /etc/hosts
cat <<-EOF > /etc/hosts
127.0.0.1   localhost
::1         localhost
127.0.1.1   ${HOST_NAME}.lan ${HOST_NAME}
EOF

systemctl enable NetworkManager

sed -i 's/^HOOKS.*$/HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block lvm2 filesystems fsck)/g' /etc/mkinitcpio.conf
mkinitcpio -p linux

umount /boot
mount /boot
bootctl install

cat <<-EOF > /boot/loader/loader.conf
default arch
timeout 4
editor  0
EOF

cat <<-EOF > /boot/loader/entries/arch-system.conf
title   Arch Linux
linux   /vmlinuz-linux
initrd  /initramfs-linux.img
options root=/dev/mapper/${VOLUME_GROUP}-root rw
EOF
ls

echo "KEYMAP=sv-latin1" > /etc/vconsole.conf
echo "FONT_MAP=8859-1" >> /etc/vconsole.conf

add_sudo_user

curl -s $URL/$BRANCH/install-yay.sh | bash

cat <<-EOF

Exekvera följnade kommandon:

1. arch-chroot /mnt
2. passwd $SUDO_USER
3. exit
4. umount -R /mnt
5. reboot
EOF
