# Arch Linux

Detta installerar Arch Linux med LVM. Det är en installation utan grafiskt gränssnitt. GIvet att denna dokumentation fortfarande är korrekt så skapas 3 logiska volymer - `root`, `swap` och `home`.

## Förberedelser

Scriptet förlitar sig på att följande variabler har tilldelats värden:

* `export URL=https://gitlab.com/jimmy.ljungberg/arch-linux-install/-/raw`
* `export BRANCH=main`
* `export STORAGE_DEVICE` - Tex `/dev/nvme0n1`, `/dev/sda`, `dev/vda`
* `export SUDO_USER` - Namn på användare som tilldelas sudo-rättigheter

Utöver ovanstående variabler finns även följande:

* `BOOT_PARTITION` - Heltal som identifierar boot-partition. Standardvärde är `1`  
  **OBSERVERA** Ändra till `p1` om `STORAGE_DEVICE` är `/dev/nvme0n1`
* `LVM_PARTITION` - Heltal som identifierar LVM-partition. Standardvärde är `2`  
  **OBSERVERA** Ändra till `p2` om `STORAGE_DEVICE` är `/dev/nvme0n1`
* `VOLUME_GROUP` - Namn på volymgrupp. Standardvärde `system`
* `HOST_NAME` - Namn på datorn. Standardvärde är `pc`

## Installation

1. Aktivera svenskt tangentbord:  
   ```bash
   loadkeys sv-latin1
   ```
1. Exekvera script:  
   ```bash
   curl --silent $URL/$BRANCH/install-1.sh | bash
   ```

## Konfiguration efter installation

## Utöka diskutrymme

Grundinstallationen är väldigt snålt inställd och inte mycket ledigt utrymme finns. Det är väldigt troligt att du behöver utöka storleken på den logiska volymerna.

### Expandera en logisk volym
```bash
sudo lvextend -L +<storlek> /dev/$VOLUME_GROUP/<logisk volym>
resize2fs /dev/$VOLUME_GROUP/<logisk volym>
```

### Powerline
```bash
curl -s $URL/$BRANCH/install-powerline.sh | bash
```

Bekräfta att scriptet lagt till ett typsnitt i filen `/etc/vconsole.conf`, om inte - kör scriptet igen. Efter omstart kommer den virtuella konsollen använda det nya typsnittet.

### Konfigurera bash
Se [bash](https://gitlab.com/jimmy.ljungberg/bash)