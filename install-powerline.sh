#!/usr/bin/env bash
set -eu

function install_powerline_fonts() {
    cd /tmp
    rm -fr powerline-fonts-git
    git clone https://aur.archlinux.org/powerline-fonts-git.git
    cd powerline-fonts-git
    makepkg --syncdeps
    sudo pacman -U --noconfirm $(ls -1 powerline-fonts-git*.zst)
}

sudo pacman --sync --needed --noconfirm \
  powerline

install_powerline_fonts

sudo sh -c 'echo "FONT=ter-powerline-v22b" >> /etc/vconsole.conf'
